package com.example.routes

//totes les rutes que tindra el servidor, totes les crides del client per llegir/mod les dades
import com.example.models.Customer
import com.example.models.customerStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.customerRouting() {
    route("/customers") {
        //tota la info
        get {
            if (customerStorage.isNotEmpty()) {
                call.respond(customerStorage) //respond() retorna l'objecte
            } else {
                call.respondText("No customers found", status = HttpStatusCode.OK)
            //responText() missatge, estado de la consulta OK porque esta bien pero solo esta vacia, si fuese error ponemos estado error
            }
        }
        //un en concret
        get("{id?}") {
            //return get per a que no segueixi el codi, es un return de la funcio
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText (
                "Missing id", status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in customerStorage) {
                if (customer.id == id) return@get call.respond("customer")
            }
            call.respondText( "No customer with id $id", status = HttpStatusCode.NotFound)



            //mirar que existeixi el parametre (si envia espais en blanc, si id es int i passa string)mensaje BAD REQUEST
            //Si id existe ->devuelve cliente
            //Si no ->mensaje NOT FOUD 404
        }
        post {
            val customer = call.receive<Customer>()
            customerStorage.add(customer)
            call.respondText("Customer stored correctly", status = HttpStatusCode.Created)
        }

        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText (
                "Missing id", status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (customer in customerStorage) {
                if (customer.id == id) {
                    customerStorage.remove(customer)
                    return@delete call.respondText("Customer removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText( "No customer with id $id", status = HttpStatusCode.NotFound)
        }
    }
}

